import json
import xml.etree.ElementTree as ET
from http.client import responses
import urllib3
import sys
import argparse
import datetime as dt

"""Template for our config"""
class Serializable:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)

class TypeKvp(Serializable):
    def __init__(self, terms:str):
        self.terms = terms


class CustomType(Serializable):
    def __init__(self, ref:str, kvp=None):
        if kvp is None:
            kvp = {}
        self.ref = ref
        self.kvp = kvp

class CustomResolver(Serializable):
    def __init__(self, prefix:str, regexp:str, kvp=None):
        if kvp is None:
            kvp = {}
        self.prefix = prefix
        self.regexp = regexp # can be None
        self.kvp = kvp # object, can be iri, terms etc..


class Tab(Serializable):
    def __init__(self, name, attrs, category=""):
        self.name = name,
        self.category = category
        self.attrs = attrs

class Attribute(Serializable):
    def __init__(self, ref:str, name:str, descr:str):
        self.ref = ref
        self.name = name
        self.alternateNames = []
        self.descr = descr
        self.suffix = ""
        self.resolvers = []
        self.forceResolver = False
        self.type = ""
        self.forceType = False
        self.virtual = False
        self.hidden = False
        self.automatic = False
        self.sticky = False


class Config(Serializable):
    def __init__(self,
                 _id: str,  # current date
                 name: str,
                 description: str
                 ):
        self._id = _id
        self.name = name
        self.description = description
        self.contextUrl = ""
        self.dmp = ""
        self.schema = ""
        self.latestOnly = False
        self.releaseFromExternal = False
        self.grantReadAccess = "anonymous"
        self.header = "header"
        self.valueSeparator = "\t"
        self.subValueSeparator = "|"
        self.tableAttributes = []
        self.tabs = []
        self.attrs = []
        self.customResolvers = []
        self.customTypes = []

class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'toJSON'):
            return obj.toJSON()
        else:
            return json.JSONEncoder.default(self, obj)

def set_value(val, default):
    if val is not None:
        return val.text
    else:
        return default


def check_if_mandatory(val):
    if val is not None:
        if val.text == 'mandatory':
            return True
    else:
        return False

def get_tab_list(root):
    for field in root.findall('./CHECKLIST/DESCRIPTOR/FIELD_GROUP/FIELD'):
        pass

"""Get field NAME, LABEL, DESCRIPTION, TYPE (regex if present), MANDATORY (convert to bool), MULTIPLICITY?"""
def get_attr_list(root):
    resolvers, attrs, custom_types = [], [], [] #dict for resolvers, id:Resolver(prefix,regexp,kvp)
    for field in root.findall('./CHECKLIST/DESCRIPTOR/FIELD_GROUP/FIELD'):
        attr_ref = field.find("./LABEL")
        attr_name = field.find("./NAME")
        attr_descr = field.find("./DESCRIPTION")
        force_type = check_if_mandatory(field.find("./MANDATORY"))
        suffix = [s.text for s in field.findall("./UNITS/UNIT")]
        resolver_regex = field.find("./FIELD_TYPE/TEXT_FIELD/REGEX_VALUE")
        custom_type_terms = [v.find("./VALUE").text for v in field.findall("./FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE")]

        if resolver_regex is not None:
            resolvers.append(CustomResolver(attr_ref.text, resolver_regex.text))

        if len(custom_type_terms) > 0:
            terms = "|".join(custom_type_terms)
            custom_types.append(CustomType(attr_ref.text,TypeKvp(terms)))

        attr = Attribute(ref=attr_ref.text,name=attr_name.text,descr=attr_descr.text)
        attr.forceType = force_type
        attr.suffix = suffix
        attrs.append(attr)
    return resolvers, attrs, custom_types

'TODO: Check for None before calling .text'
def parse_xml(path,output):
    tree = ET.parse(path)
    root = tree.getroot()
    config_id = dt.datetime.today().strftime('%Y-%m-%d')
    name = root.find('./CHECKLIST/DESCRIPTOR/NAME')
    descr = root.find('./CHECKLIST/DESCRIPTOR/DESCRIPTION')
    conf_obj = Config(config_id, name.text, descr.text)
    resolver_lst, attr_lst, custom_type_lst = get_attr_list(root)
    conf_obj.customResolvers = resolver_lst
    conf_obj.attrs = attr_lst
    conf_obj.customTypes = custom_type_lst
    json_str = conf_obj.toJSON()
    with open(output,"w") as jsonFile:
        jsonFile.write(json_str)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', dest='input', default='', help='input file')
    parser.add_argument('-o', dest='output', default='', help='output file')
    args = parser.parse_args()

    parse_xml(args.input,args.output)


if __name__ == '__main__':
    main(sys.argv[1:])
